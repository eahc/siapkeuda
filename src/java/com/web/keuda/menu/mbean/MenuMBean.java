package com.web.keuda.menu.mbean;

import com.web.utilities.SessionUtil;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author akira
 */
@ManagedBean(name = "menuMBean")
@ViewScoped
public class MenuMBean implements Serializable {
private MenuModel model = new DefaultMenuModel();
private MenuModel modelDashboard = new DefaultMenuModel();
private String user;
private String image;
    @PostConstruct
    void init() {
        model.addElement(new DefaultMenuItem("PILIH MODUL", "", "/dashboard.jsf"));
        model.addElement(dataMaster("Data Master","/master/data/"));
        model.addElement(menuMaster("Menu Master","/master/menu/"));
        user = SessionUtil.getCookieUsername().toUpperCase();
        
        
    }  
    
    private DefaultSubMenu dataMaster(String menuName,String url){
        DefaultSubMenu datamaster = new DefaultSubMenu(menuName, "layout-menuitem-toggler fa fa-fw fa-angle-down");
//        datamaster.addElement(subMasterUser("Master User", url+"masteruser/")); //SUB MASTER
        addMenuItem(datamaster, "Master User", url);
        addMenuItem(datamaster, "Master Module", url);
        addMenuItem(datamaster, "Master Group", url);
        return datamaster;
    }
    
    private DefaultSubMenu menuMaster(String menuName,String url){
        DefaultSubMenu datamaster = new DefaultSubMenu(menuName, "layout-menuitem-toggler fa fa-fw fa-angle-down");
//        datamaster.addElement(subMasterUser("Master User", url+"masteruser/")); //SUB MASTER
        addMenuItem(datamaster, "Master Menu", url);
        addMenuItem(datamaster, "Config Menu", url);
        return datamaster;
    }

    
//    SUB MENU LV 1 =================================================================================
    
    private DefaultSubMenu subMasterUser(String menuName,String url){
        DefaultSubMenu datamaster = new DefaultSubMenu(menuName,"layout-menuitem-toggler fa fa-fw fa-angle-down");
        addMenuItem(datamaster, "User Account", url);
        addMenuItem(datamaster, "User Modul", url);
        addMenuItem(datamaster, "User Group", url);
        return datamaster;
    }

    
    private void addMenuItem(DefaultSubMenu parentMenu, String name, String url) {
        DefaultMenuItem item = new DefaultMenuItem(name);
        item.setUrl(url + name.replace(' ', '_').toLowerCase() + ".jsf");
        parentMenu.addElement(item);
    }
    


    public MenuModel getModel() {
        return model;
    }

    public void setModel(MenuModel model) {
        this.model = model;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public MenuModel getModelDashboard() {
        return modelDashboard;
    }

    public void setModelDashboard(MenuModel modelDashboard) {
        this.modelDashboard = modelDashboard;
    }
    
    

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }   

    public void klickProfile(){
        
    }
    
}
