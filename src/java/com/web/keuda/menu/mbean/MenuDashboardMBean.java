/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.keuda.menu.mbean;


import com.web.keuda.session.IMenuMasterSession;
import com.web.keuda.session.MenuMasterSession;
import com.web.menu.model.MenuMaster;
import com.web.utilities.SessionUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author acer
 */
@ManagedBean(name = "menuDashboardMBean")
@ViewScoped
public class MenuDashboardMBean implements Serializable{
    private MenuModel model = new DefaultMenuModel();
    private MenuModel modelDashboard = new DefaultMenuModel();
    private List<MenuMaster> listMenu = new ArrayList<>();
    private List<MenuMaster> listSubMenu = new ArrayList<>();
    private MenuMaster masterMenu;
    private String user;
    private String userType;
    private String image;
    private String dbref;
    @EJB
    IMenuMasterSession ims = new MenuMasterSession();
    @PostConstruct
    void init() {
        try {
            dbref = SessionUtil.getCookieDBRef();
            userType = SessionUtil.getCookieEmployID();
            listMenu = ims.getMasterMenu(dbref);
            model.addElement(new DefaultMenuItem("Dashboard", "", "/dashboard.jsf"));
            for (MenuMaster masterMenu1 : listMenu) {
                model.addElement(dataMaster(masterMenu1));
            }
            user = SessionUtil.getCookieUsername().toUpperCase();
        } catch (Exception ex) {
            Logger.getLogger(MenuDashboardMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }  
    
    private DefaultSubMenu dataMaster(MenuMaster masterMenu){
        DefaultSubMenu datamaster = new DefaultSubMenu(masterMenu.getMenuname(),
                "layout-menuitem-toggler fa fa-fw fa-angle-down");
        try {
            
            listSubMenu = ims.getMasterMenuStatus(String.valueOf(masterMenu.getMasterkey()),
                    masterMenu.getLink(),SessionUtil.getCookieLogin(),dbref);
            
            for (MenuMaster masterMenu1 : listSubMenu) {
                if (masterMenu1.isIsmenuitem()) {
                    
                    datamaster.addElement(dataMaster(masterMenu1));
                    
                }else{
                    addMenuItem(datamaster, masterMenu1.getMenuname(), masterMenu1.getLink());
                }
                
            }            
            

        } catch (Exception ex) {
            Logger.getLogger(MenuDashboardMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datamaster;
    }
   
    
//    SUB MENU LV 1 =================================================================================
    
    
    private void addMenuItem(DefaultSubMenu parentMenu, String name, String url) {
        DefaultMenuItem item = new DefaultMenuItem(name);
        item.setUrl(url + name.replace(' ', '_').toLowerCase() + ".jsf");
        parentMenu.addElement(item);
    }
    


    public MenuModel getModel() {
        return model;
    }

    public void setModel(MenuModel model) {
        this.model = model;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public MenuModel getModelDashboard() {
        return modelDashboard;
    }

    public void setModelDashboard(MenuModel modelDashboard) {
        this.modelDashboard = modelDashboard;
    }

    public List<MenuMaster> getListMenu() {
        return listMenu;
    }

    public void setListMenu(List<MenuMaster> listMenu) {
        this.listMenu = listMenu;
    }

    public List<MenuMaster> getListSubMenu() {
        return listSubMenu;
    }

    public void setListSubMenu(List<MenuMaster> listSubMenu) {
        this.listSubMenu = listSubMenu;
    }

    public MenuMaster getMasterMenu() {
        return masterMenu;
    }

    public void setMasterMenu(MenuMaster masterMenu) {
        this.masterMenu = masterMenu;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getDbref() {
        return dbref;
    }

    public void setDbref(String dbref) {
        this.dbref = dbref;
    }

    public IMenuMasterSession getIms() {
        return ims;
    }

    public void setIms(IMenuMasterSession ims) {
        this.ims = ims;
    }
    
    

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }   

    public void klickProfile(){
        
    }
}
