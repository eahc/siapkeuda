/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.keuda.master.mbean;

import com.web.keuda.session.AccountAuthenticationSession;
import com.web.keuda.session.GroupAuthenticationSession;
import com.web.keuda.session.IAccountAuthenticationSession;
import com.web.keuda.session.IGroupAuthenticationSession;
import com.web.keuda.session.IMenuMasterSession;
import com.web.keuda.session.IModulAuthenticationSession;
import com.web.keuda.session.MenuMasterSession;
import com.web.keuda.session.ModulAuthenticationSession;
import com.web.login.model.UserAccount;
import com.web.menu.model.AppModule;
import com.web.menu.model.MenuMaster;
import com.web.user.model.UserGroup;
import com.web.utilities.MessageDialog;
import com.web.utilities.SessionUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.NodeUnselectEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author acer
 */
@ManagedBean(name = "masterMenuMBean")
@ViewScoped
public class MasterMenuMBean implements Serializable{
    private int masterkey, appmodule,level;
    private TreeNode selectedNode;
    private TreeNode root;
    private String menuname,description, link,dbref,menuitem; 
    private String menuname2,description2, link2,dbref2,menuitem2; 
    private boolean ismenuitem; 
    private boolean ismenuitem2; 
    private List<MenuMaster> listMenuMaster=new ArrayList<>();
    private MenuMaster menuMaster;
    private MenuMaster selectedMenuMaster;
    private AppModule appModule;
    private List<AppModule> listAppModule = new ArrayList<>();
    private UserGroup userGroup;
    private List<UserGroup> listUserGroup = new ArrayList<>();
    private List<UserAccount> listUserAccount = new ArrayList<>();
    private final FacesContext fc = FacesContext.getCurrentInstance();
    private final ExternalContext excontext = fc.getExternalContext();
    @EJB                   
    IMenuMasterSession menusession = new MenuMasterSession();
    IModulAuthenticationSession modulesession = new ModulAuthenticationSession();
    IGroupAuthenticationSession groupsession = new GroupAuthenticationSession();
    IAccountAuthenticationSession accountsession = new AccountAuthenticationSession();
    
    @PostConstruct
    void init(){
        try {
            dbref = SessionUtil.getCookieDBRef();
            listUserAccount = accountsession.getRef();
            listAppModule = modulesession.getAllModule(dbref);
            listMenuMaster = menusession.getMasterMenu(dbref);
            
            root = new DefaultTreeNode("");
            for (MenuMaster masterMenu1 : listMenuMaster) {
                TreeNode no = new DefaultTreeNode(masterMenu1, root);
                boolean cekSub = menusession.cekSubMenu(masterMenu1.getMasterkey(), dbref);
                if(cekSub){
                    submenu(masterMenu1, no);
                }
            }
            
               
        } catch (Exception ex) {
            Logger.getLogger(MasterMenuMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    void submenu(MenuMaster mm, TreeNode node){
        try {
            listMenuMaster = menusession.getMenu((int)mm.getMasterkey(),dbref);
            for (MenuMaster masterMenu : listMenuMaster) {
                TreeNode tree = new DefaultTreeNode(masterMenu, node);
                boolean cekSub = menusession.cekSubMenu(masterMenu.getMasterkey(), dbref);
                    if(cekSub){
                        submenu(masterMenu, tree);
                    }
            }

        } catch (Exception ex) {
            Logger.getLogger(MasterMenuMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void onRowEdit(RowEditEvent event){
        TreeNode node = (TreeNode)event.getObject();
        MenuMaster mg = (MenuMaster)node.getData();
        try {
            if (dbref !=null) {
                menusession.updateMenu(mg,dbref);
                
                MessageDialog.addMessageInfo("Menu "+mg.getMenuname()+" Sudah diUbah");
            }else{
                MessageDialog.addMessageWarn("DB Ref belum dipilih \nEdit dibatalkan");
            }
        } catch (Exception e) {
        }
    }
    
    public void onRowCancel(RowEditEvent event){
        
    }
    
    public void onCreateParentMenu(){
        
        try {
            if (dbref !=null) {
                masterkey = (menusession.getMasterkeyMenu(dbref) + 1);
                MenuMaster mm = new MenuMaster(masterkey, appmodule, menuname, ismenuitem, link, description, 1);
                menusession.saveMenu(mm, dbref);
                new DefaultTreeNode(mm, root);
                MessageDialog.addMessageInfo("Berhasil menyimpan Parent Menu baru "+menuname);
            }else{
                MessageDialog.addMessageWarn("DB Ref belum dipilih \nPenyimpanan dibatalkan");
            }
            
        } catch (Exception ex) {
            Logger.getLogger(MasterGroupMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void onCreateSubMenu(){
        MenuMaster mm = (MenuMaster)selectedNode.getData();
        try {
            if (dbref !=null) {
                if (mm.isIsmenuitem() == false) {
                    menusession.updateIsmenuItem((int)mm.getMasterkey(),true, dbref);
                    masterkey = (menusession.getMasterkeyMenu(dbref) + 1);
                    MenuMaster mp = new MenuMaster(masterkey, mm.getAppmodul(),mm.getMasterkey(), menuname2, ismenuitem2, 
                            link2, description2, (mm.getLevel())+1);
                    menusession.saveMenu(mp, dbref);
                    new DefaultTreeNode(mp,selectedNode);
                }else{
                    masterkey = (menusession.getMasterkeyMenu(dbref) + 1);
                    MenuMaster mp = new MenuMaster(masterkey, mm.getAppmodul(),mm.getMasterkey(), menuname2, ismenuitem2, 
                            link2, description2, (mm.getLevel())+1);
                    menusession.saveMenu(mp, dbref);
                    new DefaultTreeNode(mp, selectedNode);
                }
                MessageDialog.addMessageInfo("Berhasil menyimpan Sub Menu baru "+menuname);
            }else{
                MessageDialog.addMessageWarn("DB Ref belum dipilih \nPenyimpanan dibatalkan");
            }
            
        } catch (Exception ex) {
            Logger.getLogger(MasterMenuMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void onDelete(){
        MenuMaster mm = (MenuMaster)selectedNode.getData();
         try {
             boolean cekParent = menusession.cekParentMenu(mm.getMasterkey(), dbref);
            if (dbref ==null ) {
                MessageDialog.addMessageWarn("DB Ref belum dipilih \nPenghapusan dibatalkan");
            }else if(cekParent){
                MessageDialog.addMessageWarn("Menu memiliki submenu \nHapus Submenu satupersatu submenu ");
            }else{
                long sub = menusession.getParentMenu(mm.getMasterkey(), dbref);
                menusession.deleteMenu((int)mm.getMasterkey(), dbref);
                boolean cekparen2 = menusession.cekParentMenu(sub, dbref);
                if(cekparen2 == false){
                    menusession.updateIsmenuItem((int)sub, false, dbref);
                    
                }
                selectedNode.getParent().getChildren().remove(selectedNode);
                MessageDialog.addMessageInfo("Menu "+mm.getMenuname()+" Sudah dihapus");
            }
        } catch (Exception ex) {
            Logger.getLogger(MasterMenuMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onClear(){
        setMenuname("");
        setMenuname("");
        setDescription("");
        setAppmodule(0);
        setLink("");
        setLevel(0);
        setIsmenuitem(false);
    }
    
    public void onChange(){
        System.out.println("DB REF "+dbref);
        if (dbref != null) {
            try {
            listMenuMaster = menusession.getMasterMenu(dbref);
            root = new DefaultTreeNode("");
            for (MenuMaster masterMenu1 : listMenuMaster) {
                TreeNode no = new DefaultTreeNode(masterMenu1, root);
            }             
                for (MenuMaster menuMaster1 : listMenuMaster) {
                    if (menuMaster1.isIsmenuitem()) {
                        menuMaster1.setMenuitem("Ada");
                    }else{
                        menuMaster1.setMenuitem("Tidak Ada");
                    }
                }
            } catch (Exception ex) {
                Logger.getLogger(MasterGroupMBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
  
 
    public void onNodeCollapse(NodeCollapseEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Collapsed", event.getTreeNode().toString());
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
 
 
    public void onNodeUnselect(NodeUnselectEvent event) {
            
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Unselected", event.getTreeNode().toString());
            FacesContext.getCurrentInstance().addMessage(null, message);
        
    }

    public int getMasterkey() {
        return masterkey;
    }

    public void setMasterkey(int masterkey) {
        this.masterkey = masterkey;
    }

    public int getAppmodule() {
        return appmodule;
    }

    public void setAppmodule(int appmodule) {
        this.appmodule = appmodule;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getMenuname() {
        return menuname;
    }

    public void setMenuname(String menuname) {
        this.menuname = menuname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDbref() {
        return dbref;
    }

    public void setDbref(String dbref) {
        this.dbref = dbref;
    }

    public boolean isIsmenuitem() {
        return ismenuitem;
    }

    public void setIsmenuitem(boolean ismenuitem) {
        this.ismenuitem = ismenuitem;
    }

    public List<MenuMaster> getListMenuMaster() {
        return listMenuMaster;
    }

    public void setListMenuMaster(List<MenuMaster> listMenuMaster) {
        this.listMenuMaster = listMenuMaster;
    }

    public MenuMaster getMenuMaster() {
        return menuMaster;
    }

    public void setMenuMaster(MenuMaster menuMaster) {
        this.menuMaster = menuMaster;
    }

    public MenuMaster getSelectedMenuMaster() {
        return selectedMenuMaster;
    }

    public void setSelectedMenuMaster(MenuMaster selectedMenuMaster) {
        this.selectedMenuMaster = selectedMenuMaster;
    }

    public AppModule getAppModule() {
        return appModule;
    }

    public void setAppModule(AppModule appModule) {
        this.appModule = appModule;
    }

    public List<AppModule> getListAppModule() {
        return listAppModule;
    }

    public void setListAppModule(List<AppModule> listAppModule) {
        this.listAppModule = listAppModule;
    }

    public UserGroup getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(UserGroup userGroup) {
        this.userGroup = userGroup;
    }

    public List<UserGroup> getListUserGroup() {
        return listUserGroup;
    }

    public void setListUserGroup(List<UserGroup> listUserGroup) {
        this.listUserGroup = listUserGroup;
    }

    public List<UserAccount> getListUserAccount() {
        return listUserAccount;
    }

    public void setListUserAccount(List<UserAccount> listUserAccount) {
        this.listUserAccount = listUserAccount;
    }

    public String getMenuitem() {
        return menuitem;
    }

    public void setMenuitem(String menuitem) {
        this.menuitem = menuitem;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public String getMenuname2() {
        return menuname2;
    }

    public void setMenuname2(String menuname2) {
        this.menuname2 = menuname2;
    }

    public String getDescription2() {
        return description2;
    }

    public void setDescription2(String description2) {
        this.description2 = description2;
    }

    public String getLink2() {
        return link2;
    }

    public void setLink2(String link2) {
        this.link2 = link2;
    }

    public String getDbref2() {
        return dbref2;
    }

    public void setDbref2(String dbref2) {
        this.dbref2 = dbref2;
    }

    public String getMenuitem2() {
        return menuitem2;
    }

    public void setMenuitem2(String menuitem2) {
        this.menuitem2 = menuitem2;
    }

    public boolean isIsmenuitem2() {
        return ismenuitem2;
    }

    public void setIsmenuitem2(boolean ismenuitem2) {
        this.ismenuitem2 = ismenuitem2;
    }
    
    
    
    
}
