/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.keuda.master.mbean;

import com.web.keuda.session.AccountAuthenticationSession;
import com.web.keuda.session.GroupAuthenticationSession;
import com.web.keuda.session.IAccountAuthenticationSession;
import com.web.keuda.session.IGroupAuthenticationSession;
import com.web.login.model.UserAccount;
import com.web.user.model.UserGroup;
import com.web.utilities.MessageDialog;
import com.web.utilities.SessionUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author acer
 */
@ManagedBean(name = "masterGroupMBean")
@ViewScoped
public class MasterGroupMBean implements Serializable{
    private List<UserGroup> listGroup = new ArrayList<>();
    private List<UserAccount> listAccount = new ArrayList<>();
    private UserGroup userGroup,selectedUserGroup;
    private int masterkey;
    private boolean check,renderOp1,renderOp2;
    private String grupname,description,dbref;
    
    @EJB
    IGroupAuthenticationSession groupsession = new GroupAuthenticationSession();
    IAccountAuthenticationSession accountsession = new AccountAuthenticationSession();
    
    @PostConstruct
    void init(){
        renderOp1 = true;
        renderOp2 = false;
        try {
            dbref = SessionUtil.getCookieDBRef();
            listAccount = accountsession.getRef();
            listGroup = groupsession.getAllGroup(dbref);
        } catch (Exception ex) {
            Logger.getLogger(MasterGroupMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onRowEdit(RowEditEvent event){
        UserGroup mg = (UserGroup)event.getObject();
        try {
            if (dbref !=null) {
                groupsession.updateGroup(mg,dbref);
                init();
                MessageDialog.addMessageInfo("Group "+grupname+" Sudah diUbah");
            }else{
                MessageDialog.addMessageWarn("DB Ref belum dipilih \nEdit dibatalkan");
            }
        } catch (Exception e) {
        }
    }
    public void onRowCancel(RowEditEvent event){
        
    }
    
    public void onSelectedTable(SelectEvent even){
        
    }
    
    public void onCreate(){
        try {
            if (dbref !=null) {
                masterkey = (groupsession.getMasterkeyGroup(dbref) + 1);
                userGroup = new UserGroup(masterkey, grupname, description);
                groupsession.simpanGroup(userGroup, dbref);
                init();
                MessageDialog.addMessageInfo("Berhasil menyimpan Group baru "+grupname);
            }else{
                MessageDialog.addMessageWarn("DB Ref belum dipilih \nPenyimpanan dibatalkan");
            }
            
        } catch (Exception ex) {
            Logger.getLogger(MasterGroupMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onDelete(){
         try {
            if (dbref !=null) {
                groupsession.hapusGroup(selectedUserGroup.getMasterkey(), dbref);
                init();
                MessageDialog.addMessageInfo("Group "+grupname+" Sudah dihapus");
            }else{
                MessageDialog.addMessageWarn("DB Ref belum dipilih \nPenghapusan dibatalkan");
                
            }
        } catch (Exception ex) {
            Logger.getLogger(MasterGroupMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onClear(){
        setGrupname("");
        setDescription("");
    }
    
    public void onChange(){
        if (dbref != null) {
            try {
            listGroup = groupsession.getAllGroup(dbref);
            } catch (Exception ex) {
                Logger.getLogger(MasterGroupMBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }

    public List<UserGroup> getListGroup() {
        return listGroup;
    }

    public void setListGroup(List<UserGroup> listGroup) {
        this.listGroup = listGroup;
    }

    public UserGroup getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(UserGroup userGroup) {
        this.userGroup = userGroup;
    }

    public UserGroup getSelectedUserGroup() {
        return selectedUserGroup;
    }

    public void setSelectedUserGroup(UserGroup selectedUserGroup) {
        this.selectedUserGroup = selectedUserGroup;
    }

    public int getMasterkey() {
        return masterkey;
    }

    public void setMasterkey(int masterkey) {
        this.masterkey = masterkey;
    }

    public String getGrupname() {
        return grupname;
    }

    public void setGrupname(String grupname) {
        this.grupname = grupname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public IGroupAuthenticationSession getGroupsession() {
        return groupsession;
    }

    public void setAuth(IGroupAuthenticationSession groupsession) {
        this.groupsession = groupsession;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public boolean isRenderOp1() {
        return renderOp1;
    }

    public void setRenderOp1(boolean renderOp1) {
        this.renderOp1 = renderOp1;
    }

    public boolean isRenderOp2() {
        return renderOp2;
    }

    public void setRenderOp2(boolean renderOp2) {
        this.renderOp2 = renderOp2;
    }

    public List<UserAccount> getListAccount() {
        return listAccount;
    }

    public void setListAccount(List<UserAccount> listAccount) {
        this.listAccount = listAccount;
    }

    public String getDbref() {
        return dbref;
    }

    public void setDbref(String dbref) {
        this.dbref = dbref;
    }
    
    
    
    
}
