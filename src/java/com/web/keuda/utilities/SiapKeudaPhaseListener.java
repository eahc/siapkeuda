package com.web.keuda.utilities;

import com.web.keuda.session.IUserAuthenticationSession;
import com.web.keuda.session.UserAuthenticationSession;
import com.web.menu.model.AppModule;
import com.web.utilities.SessionUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletRequest;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;

/**
 *
 * @author Akira
 */
public class SiapKeudaPhaseListener implements PhaseListener{
    
    private List<AppModule> ListAppModule = new ArrayList<>();    
    private AppModule AppModule = new AppModule();
    @EJB
    IUserAuthenticationSession auth = new UserAuthenticationSession();
    @Override
    public void afterPhase(PhaseEvent event) {
//        boolean validation = false;
        boolean cekgroup = false;
        FacesContext fc = event.getFacesContext();
        ExternalContext excontext = fc.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) excontext.getRequest();
        String URI = request.getRequestURI();
        if (URI.endsWith("index.jsf")) {
            String username = request.getParameter("username");
            String sessionid = request.getParameter("sessionid");
            String dbref = request.getParameter("dbref");
            String webref = request.getParameter("webref");
            String appref = request.getParameter("appref");
            String email = request.getParameter("email");
            String usertype = request.getParameter("usertype");
            String employid = request.getParameter("employid");
            String master = request.getParameter("master");
            try {
                if(sessionid != null || SessionUtil.getCookieSessionID()!=null){
                    
                    if(SessionUtil.getCookieSessionID()==null || SessionUtil.getCookieSessionID().equals("")){
                        SessionUtil.setUsernameCookie(username);
                        SessionUtil.setSessionIDCookie(sessionid);
                        SessionUtil.setDBRefCookie(dbref);
                        SessionUtil.setWebRefCookie(webref);
                        SessionUtil.setEmployIDCookie(usertype);
                        SessionUtil.setLoginCookie(master);
                        
                    }
                    boolean validation = auth.getValidateUser(username,dbref);
                            if(validation){
                                
                                ListAppModule = auth.cekModule(username,dbref);
                                
                                if(ListAppModule.size()>1){ 
                                    
                                    excontext.redirect("dashboard.jsf");
                                    
                                }else
                                if(ListAppModule.size()==1){
                                    
                                    for (AppModule mod : ListAppModule) {
                                        
                                        excontext.redirect(mod.getLink());
                                    }
                                    
                                }else{
                                SessionUtil.deleteCookies();
                                
                                String msg = "Maaf !!! Group Anda belum memiliki modul \n"
                                        + "Silahkan hubungi Administrator";
                                excontext.redirect("/SiapKeuda-Login/login.jsf?message='"+msg+"'");
                                }
                            }else{
                                SessionUtil.deleteCookies();
                                
                                String msg = "Maaf !!! \nProses validasi user tidak sesuai\n"
                                        + "Silahkan hubungi Administrator";
                                excontext.redirect("/SiapKeuda-Login/login.jsf?message='"+msg+"'");
                            }
                  
                } else {
                    excontext.redirect("/SiapKeuda-Login/login.jsf?keuda=false");
                }
            } catch (Exception ex) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Kesalahan Proses Verifikasi User Antar Server", ex.toString());
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        }else {
            String username = SessionUtil.getCookieUsername();
            if(username != null){
                
            } else {
                try {
                    excontext.redirect("/SiapKeuda-Login/login.jsf?keuda=false");
                } catch (IOException ex) {
                    Logger.getLogger(SiapKeudaPhaseListener.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
       
    }

    @Override
    public void beforePhase(PhaseEvent event) {
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }
    
}
