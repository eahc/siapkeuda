/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.keuda.master.mbean;

import com.web.keuda.session.AccountAuthenticationSession;
import com.web.keuda.session.IAccountAuthenticationSession;
import com.web.keuda.session.IModulAuthenticationSession;
import com.web.keuda.session.ModulAuthenticationSession;
import com.web.login.model.UserAccount;
import com.web.menu.model.AppModule;
import com.web.user.model.UserGroup;
import com.web.utilities.MessageDialog;
import com.web.utilities.SessionUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author acer
 */
@ManagedBean(name = "masterModuleMBean")
@ViewScoped
public class MasterModuleMBean implements Serializable{
    private AppModule masterModule;
    private List<AppModule> listMasterModule = new ArrayList<>();
    private List<UserAccount> listAccount = new ArrayList<>();
    private AppModule selectedMasterModule;
    private int masterkey;
    private String modulename,description,icon,link,dbref;
    
    @EJB
    IModulAuthenticationSession modulsession = new ModulAuthenticationSession();
    IAccountAuthenticationSession accountsession = new AccountAuthenticationSession();
    
    @PostConstruct
    void init(){
        
        try {
            dbref = SessionUtil.getCookieDBRef();
            listAccount = accountsession.getRef();
            listMasterModule = modulsession.getAllModule(dbref);
        } catch (Exception ex) {
            Logger.getLogger(MasterGroupMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onRowEdit(RowEditEvent event){
        AppModule mod = (AppModule)event.getObject();
        try {
            if (dbref !=null) {
                modulsession.updateModul(mod,dbref);
                init();
                MessageDialog.addMessageInfo("Module "+modulename+" Sudah diUbah");
            }else{
                MessageDialog.addMessageWarn("DB Ref belum dipilih \nEdit dibatalkan");
            }
        } catch (Exception e) {
        }
    }
    public void onRowCancel(RowEditEvent event){
        
    }
    
    public void onSelectedTable(SelectEvent even){
        
    }
    
    public void onCreate(){
        try {
            if (dbref !=null) {
                masterkey = (modulsession.getMasterkeyModule(dbref) + 1);
                masterModule = new AppModule(masterkey, modulename, icon, link, description);
                modulsession.simpanModul(masterModule, dbref);
                init();
                MessageDialog.addMessageInfo("Berhasil menyimpan Module baru "+modulename);
            }else{
                MessageDialog.addMessageWarn("DB Ref belum dipilih \nPenyimpanan dibatalkan");
            }
            
        } catch (Exception ex) {
            Logger.getLogger(MasterGroupMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onDelete(){
         try {
            if (dbref !=null) {
                modulsession.deleteModul((int)selectedMasterModule.getMasterkey(), dbref);
                init();
                MessageDialog.addMessageInfo("Module "+modulename+" Sudah dihapus");
            }else{
                MessageDialog.addMessageWarn("DB Ref belum dipilih \nPenghapusan dibatalkan");
                
            }
        } catch (Exception ex) {
            Logger.getLogger(MasterGroupMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onClear(){
        setModulename("");
        setDescription("");
    }
    
    public void onChange(){
        if (dbref != null) {
            try {
            listMasterModule = modulsession.getAllModule(dbref);
            } catch (Exception ex) {
                Logger.getLogger(MasterGroupMBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }

    public AppModule getMasterModule() {
        return masterModule;
    }

    public void setMasterModule(AppModule masterModule) {
        this.masterModule = masterModule;
    }

    public List<AppModule> getListMasterModule() {
        return listMasterModule;
    }

    public void setListMasterModule(List<AppModule> listMasterModule) {
        this.listMasterModule = listMasterModule;
    }

    public AppModule getSelectedMasterModule() {
        return selectedMasterModule;
    }

    public void setSelectedMasterModule(AppModule selectedMasterModule) {
        this.selectedMasterModule = selectedMasterModule;
    }

    public int getMasterkey() {
        return masterkey;
    }

    public void setMasterkey(int masterkey) {
        this.masterkey = masterkey;
    }

    public String getModulename() {
        return modulename;
    }

    public void setModulename(String modulename) {
        this.modulename = modulename;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public List<UserAccount> getListAccount() {
        return listAccount;
    }

    public void setListAccount(List<UserAccount> listAccount) {
        this.listAccount = listAccount;
    }

    public String getDbref() {
        return dbref;
    }

    public void setDbref(String dbref) {
        this.dbref = dbref;
    }
    
    
    
}
