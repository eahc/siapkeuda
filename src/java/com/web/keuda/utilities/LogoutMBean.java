package com.web.keuda.utilities;

import com.web.utilities.EjbContext;
import com.web.utilities.SessionUtil;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author acer
 */
@ManagedBean(name = "logoutMbean")
@RequestScoped
public class LogoutMBean implements Serializable{
    
    public void deleteSession() throws Exception{
        SessionUtil.deleteCookies();
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext excontext = fc.getExternalContext();
        excontext.redirect(EjbContext.getBudgettingDomainComplete() +"/SiapKeuda/index.jsf");
        SessionUtil.deleteServletSession();
    }
    
    public String onOffice() {
        return EjbContext.getBudgettingDomainComplete()+"SiapKeuda/dashboard.jsf";
    }
}
