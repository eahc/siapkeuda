
PrimeFaces.locales['in'] = {
    closeText: 'Ok',
    prevText: 'Sembelumnya',
    nextText: 'Berikutnya',
    currentText: 'Hari Ini',
    monthNames: ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'],
    monthNamesShort: ['Jan','Feb','Mar','Apr','Mei','Jun', 'Jul','Agu','Sep','Okt','Nov','Des'],
    dayNames: ['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu'],
    dayNamesShort: ['Min','Sen','Sel','Rab','Kam','Jum','Sab'],
    dayNamesMin: ['M','S','S','R','K','J','S'],
    weekHeader: 'Minggu',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: '',
    timeOnlyTitle: 'Waktu',
    timeText: 'Waktu',
    hourText: 'Jam',
    minuteText: 'Menit',
    secondText: 'Detik',
    ampm: false,
    month: 'Bulan',
    week: 'Minggu',
    day: 'Hari',
    allDayText : 'Semua Hari'
};
       