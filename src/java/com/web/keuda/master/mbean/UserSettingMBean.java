package com.web.keuda.master.mbean;

import com.web.keuda.session.AccountAuthenticationSession;
import com.web.keuda.session.IAccountAuthenticationSession;
import com.web.keuda.session.IUserAuthenticationSession;
import com.web.keuda.session.UserAuthenticationSession;
import com.web.login.model.UserAccount;
import com.web.user.model.UserGroup;
import com.web.user.model.UserRef;
import com.web.utilities.Crypto;
import com.web.utilities.EjbContext;
import com.web.utilities.MasterSMTP_EMail;
import com.web.utilities.PasswordGenerator;
import com.web.utilities.SendMailActivation;
import java.io.Serializable;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author acer
 */
@ManagedBean(name = "userSettingMBean")
@ViewScoped
public class UserSettingMBean implements Serializable{
    private List<UserAccount> listUserAccount = new ArrayList<>();
    private UserAccount UserAccountSelected;
    private UserAccount userAccount;
    private List<UserRef> listUserRef = new ArrayList<>();
    private UserRef UserRefSelected;
    private UserRef userRef;
    private List<UserGroup> listUserGroup = new ArrayList<>();
    private UserGroup UserGroupSelected;
    private UserGroup UserGroup;
    private int masterkey,usertype,useractivatedby,Group;
    private String password;
    private String username,webref,dbref,appref,email,status;
    private boolean renderOp1,renderOp2,useractive;
    
    @EJB
    private IUserAuthenticationSession auth = new UserAuthenticationSession();
    private IAccountAuthenticationSession accauth = new AccountAuthenticationSession();
    
    @PostConstruct
    void init(){
            renderOp1 = true;
            renderOp2 = false;

            try {
            
                listUserAccount = accauth.getUserAccount();
                for (UserAccount userAccount1 : listUserAccount) {
                    if (userAccount1.isUseractive()) {
                        userAccount1.setStatus("Aktif");
                    }else{
                        userAccount1.setStatus("Tidak Aktif");
                    }
                }
            
        } catch (Exception ex) {
            Logger.getLogger(UserSettingMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void tambahAccount(){
            renderOp1 = false;
            renderOp2 = true;
    }
    public void cancel(){
            setRenderOp1(true);
            setRenderOp2(false);
    }
    
    public void simpan(){
        try {
            PasswordGenerator passwordGenerator = new PasswordGenerator.PasswordGeneratorBuilder()
            .useDigits(true)
            .useLower(true)
            .useUpper(true)
            .build();
            password = passwordGenerator.generate(8);
            useractive = false;
            masterkey = (accauth.getMasterkey() + 1);
            String token = UUID.randomUUID().toString();
            String url = EjbContext.getBudgettingDomainComplete()+
                    "/SiapKeuda-Login/verification/verification.jsf?id="+masterkey+"&act="+token+"";
            try {
                UserAccount uacc = new UserAccount(masterkey, username, email, Crypto.encrypt(password.getBytes()),
                        usertype, webref, appref, dbref, useractive, useractivatedby);
                try {
//                    accauth.;
                    SendMailActivation sendmail = new SendMailActivation();
                    sendmail.kirimEmail(url, email,username,password);
                    renderOp1 = true;
                    renderOp2 = false;
                } catch (Exception ex) {
                    Logger.getLogger(UserSettingMBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | NoSuchPaddingException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException ex) {
                Logger.getLogger(UserSettingMBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (Exception ex) {
            Logger.getLogger(UserSettingMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void update(){
        try {
            UserAccount uacc = new UserAccount(masterkey, username, email, Crypto.encrypt(password.getBytes()),
                    usertype, webref, appref, dbref, useractive, useractivatedby);
            try {
//                accauth.updateUserAccount(uacc,group,dbref);
                renderOp1 = true;
                renderOp2 = false;
            } catch (Exception ex) {
                Logger.getLogger(UserSettingMBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | NoSuchPaddingException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException ex) {
            Logger.getLogger(UserSettingMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void selectTable(){
        
    }
    
    public void onRowEdit(RowEditEvent event){
        UserAccount ua = (UserAccount)event.getObject();
    }
    
    public void onRowCancel(RowEditEvent event){
        UserAccount ua = (UserAccount)event.getObject();
        FacesMessage msg = new FacesMessage("Edit Cancelled",ua.getUsername());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public List<UserAccount> getListUserAccount() {
        return listUserAccount;
    }

    public void setListUserAccount(List<UserAccount> listUserAccount) {
        this.listUserAccount = listUserAccount;
    }

    public UserAccount getUserAccountSelected() {
        return UserAccountSelected;
    }

    public void setUserAccountSelected(UserAccount UserAccountSelected) {
        this.UserAccountSelected = UserAccountSelected;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    public List<UserRef> getListUserRef() {
        return listUserRef;
    }

    public void setListUserRef(List<UserRef> listUserRef) {
        this.listUserRef = listUserRef;
    }

    public UserRef getUserRefSelected() {
        return UserRefSelected;
    }

    public void setUserRefSelected(UserRef UserRefSelected) {
        this.UserRefSelected = UserRefSelected;
    }

    public UserRef getUserRef() {
        return userRef;
    }

    public void setUserRef(UserRef userRef) {
        this.userRef = userRef;
    }

    public List<UserGroup> getListUserGroup() {
        return listUserGroup;
    }

    public void setListUserGroup(List<UserGroup> listUserGroup) {
        this.listUserGroup = listUserGroup;
    }

    public UserGroup getUserGroupSelected() {
        return UserGroupSelected;
    }

    public void setUserGroupSelected(UserGroup UserGroupSelected) {
        this.UserGroupSelected = UserGroupSelected;
    }

    public UserGroup getUserGroup() {
        return UserGroup;
    }

    public void setUserGroup(UserGroup UserGroup) {
        this.UserGroup = UserGroup;
    }

    public boolean isRenderOp1() {
        return renderOp1;
    }

    public void setRenderOp1(boolean renderOp1) {
        this.renderOp1 = renderOp1;
    }

    public boolean isRenderOp2() {
        return renderOp2;
    }

    public void setRenderOp2(boolean renderOp2) {
        this.renderOp2 = renderOp2;
    }

    public IUserAuthenticationSession getAuth() {
        return auth;
    }

    public void setAuth(IUserAuthenticationSession auth) {
        this.auth = auth;
    }

    public IAccountAuthenticationSession getAccauth() {
        return accauth;
    }

    public void setAccauth(IAccountAuthenticationSession accauth) {
        this.accauth = accauth;
    }

    public int getMasterkey() {
        return masterkey;
    }

    public void setMasterkey(int masterkey) {
        this.masterkey = masterkey;
    }

    public int getUsertype() {
        return usertype;
    }

    public void setUsertype(int usertype) {
        this.usertype = usertype;
    }

    public int getUseractivatedby() {
        return useractivatedby;
    }

    public void setUseractivatedby(int useractivatedby) {
        this.useractivatedby = useractivatedby;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getWebref() {
        return webref;
    }

    public void setWebref(String webref) {
        this.webref = webref;
    }

    public String getDbref() {
        return dbref;
    }

    public void setDbref(String dbref) {
        this.dbref = dbref;
    }

    public String getAppref() {
        return appref;
    }

    public void setAppref(String appref) {
        this.appref = appref;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isUseractive() {
        return useractive;
    }

    public void setUseractive(boolean useractive) {
        this.useractive = useractive;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    
    
}
