package com.web.keuda.menu.mbean;


import com.web.keuda.session.IModulAuthenticationSession;
import com.web.keuda.session.IUserAuthenticationSession;
import com.web.keuda.session.ModulAuthenticationSession;
import com.web.keuda.session.UserAuthenticationSession;
import com.web.menu.model.AppModule;
import com.web.user.model.UserGroup;
import com.web.utilities.SessionUtil;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author acer
 */
@ManagedBean(name = "buttonModulMBean")
@ViewScoped
public class ButtonModulMBean implements Serializable{
    private String penjualan;
    private String pembelian;
    private String karyawan;
    private String type;
    private List<AppModule> masterButton=new ArrayList<>();
    private List<AppModule> masterModule=new ArrayList<>();
    private UserGroup userGroup = new UserGroup();
    private AppModule mButton;
    private String webref;
    private LineChartModel lineAnimatedModel;
    private BarChartModel barAnimatedModel;
    private PieChartModel pieAnimatedModel;
    @EJB
    private IModulAuthenticationSession auth = new ModulAuthenticationSession();
    private IUserAuthenticationSession cekGroup = new UserAuthenticationSession();
    
    @PostConstruct
    void init(){
        try {
            userGroup = cekGroup.getUserGroupById(Integer.parseInt(SessionUtil.getCookieLogin()));
            
            if ( SessionUtil.getCookieEmployID() !=null) {
                createAnimatedModels();
                penjualan = "28000";
                pembelian = "14000";
                karyawan = "Miting";

                    if (userGroup.getMasterkey() == 1) {
                        
                        masterButton = auth.getAllModule(SessionUtil.getCookieDBRef());
                        
                        masterButton.add(new AppModule("Setting","./resources/images/user_seting.png", "master/user_setting.jsf","User Setting"));
                        
                    }else if (userGroup.getMasterkey() > 1){
                        
                        masterButton = auth.getModuleByUserid(Integer.parseInt(SessionUtil.getCookieLogin()),SessionUtil.getCookieDBRef());
                    }
                
            }else{
                try {
                    FacesContext fc = FacesContext.getCurrentInstance();
                    ExternalContext excontext =  fc.getExternalContext();
                    int userid= Integer.parseInt(SessionUtil.getCookieUsername());
                    masterModule = auth.getModuleByUserid(userid,webref);
                    for (AppModule appModule : masterModule) {
                        webref = appModule.getLink();
                    }
                    excontext.redirect(webref);
                } catch (IOException ex) {
                    Logger.getLogger(ButtonModulMBean.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        } catch (Exception ex) {
            Logger.getLogger(ButtonModulMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void createAnimatedModels() {
        lineAnimatedModel = initLinearModel();
        lineAnimatedModel.setTitle("Line Chart");
        lineAnimatedModel.setAnimate(true);
        lineAnimatedModel.setLegendPosition("se");
        Axis yAxis = lineAnimatedModel.getAxis(AxisType.Y);
        yAxis.setMin(0);
        yAxis.setMax(15);
         
        barAnimatedModel = initBarModel();
        barAnimatedModel.setTitle("Bar Charts");
        barAnimatedModel.setAnimate(true);
        barAnimatedModel.setLegendPosition("ne");
        yAxis = barAnimatedModel.getAxis(AxisType.Y);
        yAxis.setMin(0);
        yAxis.setMax(250);
        
        pieAnimatedModel = initPieModel();
        
    }
     
    private BarChartModel initBarModel() {
        BarChartModel model = new BarChartModel();
 
        ChartSeries boys = new ChartSeries();
        boys.setLabel("Boys");
        boys.set("2004", 120);
        boys.set("2005", 100);
        boys.set("2006", 44);
        boys.set("2007", 150);
        boys.set("2008", 25);
 
        ChartSeries girls = new ChartSeries();
        girls.setLabel("Girls");
        girls.set("2004", 52);
        girls.set("2005", 60);
        girls.set("2006", 110);
        girls.set("2007", 135);
        girls.set("2008", 120);
 
        model.addSeries(boys);
        model.addSeries(girls);
         
        return model;
    }
     
    private LineChartModel initLinearModel() {
        LineChartModel model = new LineChartModel();
 
        LineChartSeries series1 = new LineChartSeries();
        series1.setLabel("Series 1");
 
        series1.set(1, 2);
        series1.set(2, 1);
        series1.set(3, 3);
        series1.set(4, 6);
        series1.set(5, 8);
 
        LineChartSeries series2 = new LineChartSeries();
        series2.setLabel("Series 2");
 
        series2.set(1, 6);
        series2.set(2, 3);
        series2.set(3, 2);
        series2.set(4, 7);
        series2.set(5, 9);
 
        model.addSeries(series1);
        model.addSeries(series2);
         
        return model;
    }
    
    private PieChartModel initPieModel() {
        PieChartModel pieModel2 = new PieChartModel();
         
        pieModel2.set("Brand 1", 540);
        pieModel2.set("Brand 2", 325);
        pieModel2.set("Brand 3", 702);
        pieModel2.set("Brand 4", 421);
         
        pieModel2.setTitle("Custom Pie");
        pieModel2.setLegendPosition("e");
        pieModel2.setFill(false);
        pieModel2.setShowDataLabels(true);
        pieModel2.setDiameter(200);
        
        return pieModel2;
    }
    
    public void onSubmit(String url){
        try {
            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext excontext =  fc.getExternalContext();
            excontext.redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(ButtonModulMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getPenjualan() {
        return penjualan;
    }

    public void setPenjualan(String penjualan) {
        this.penjualan = penjualan;
    }

    public String getPembelian() {
        return pembelian;
    }

    public void setPembelian(String pembelian) {
        this.pembelian = pembelian;
    }

    public String getKaryawan() {
        return karyawan;
    }

    public void setKaryawan(String karyawan) {
        this.karyawan = karyawan;
    }

    public LineChartModel getLineAnimatedModel() {
        return lineAnimatedModel;
    }

    public void setLineAnimatedModel(LineChartModel lineAnimatedModel) {
        this.lineAnimatedModel = lineAnimatedModel;
    }

    public BarChartModel getBarAnimatedModel() {
        return barAnimatedModel;
    }

    public void setBarAnimatedModel(BarChartModel barAnimatedModel) {
        this.barAnimatedModel = barAnimatedModel;
    }

    public PieChartModel getPieAnimatedModel() {
        return pieAnimatedModel;
    }

    public void setPieAnimatedModel(PieChartModel pieAnimatedModel) {
        this.pieAnimatedModel = pieAnimatedModel;
    }

    public List<AppModule> getMasterButton() {
        return masterButton;
    }

    public void setMasterButton(List<AppModule> masterButton) {
        this.masterButton = masterButton;
    }

    public List<AppModule> getMasterModule() {
        return masterModule;
    }

    public void setMasterModule(List<AppModule> masterModule) {
        this.masterModule = masterModule;
    }

    public String getWebref() {
        return webref;
    }

    public void setWebref(String webref) {
        this.webref = webref;
    }

    
    
    
}
