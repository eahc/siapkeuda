/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.keuda.master.mbean;

import com.web.keuda.session.AccountAuthenticationSession;
import com.web.keuda.session.AppMenuAuthenticationSession;
import com.web.keuda.session.GroupAuthenticationSession;
import com.web.keuda.session.IAccountAuthenticationSession;
import com.web.keuda.session.IAppMenuAuthenticationSession;
import com.web.keuda.session.IGroupAuthenticationSession;
import com.web.keuda.session.IMenuMasterSession;
import com.web.keuda.session.MenuMasterSession;
import com.web.login.model.UserAccount;
import com.web.menu.model.AppMenu;
import com.web.menu.model.MenuMaster;
import com.web.user.model.UserGroup;
import com.web.utilities.MessageDialog;
import com.web.utilities.SessionUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author acer
 */
@ManagedBean(name = "configMenuMBean")
@ViewScoped
public class ConfigMenuMBean implements Serializable{
    private List<MenuMaster> listMenuMaster = new ArrayList<>();
    private List<UserGroup> listUserGroup = new ArrayList<>();
    private List<UserAccount> listUserAccount = new ArrayList<>();
    private MenuMaster menuMaster;
    private UserGroup userGroup;
    private UserGroup selectUserGroup;
    private MenuMaster selectMenuMaster;
    private List<AppMenu> selectedMultiAppMenu;
    private List<MenuMaster> selectedMultiMasterMenu;
    private List<AppMenu> listAppMenu = new ArrayList<>();
    private AppMenu appMenu;
    private AppMenu selectAppMenu;
    private int idmenu,idusergroup;
    private String menuname,groupname,dbref;
    private boolean isread,iswrite,isprinting,isexternalaccess,renderOp1,renderOp2;
    
    @EJB
    IGroupAuthenticationSession groupsession = new GroupAuthenticationSession();
    IMenuMasterSession menusession = new MenuMasterSession();
    IAppMenuAuthenticationSession appSession = new AppMenuAuthenticationSession();
    IAccountAuthenticationSession accountsession = new AccountAuthenticationSession();
    
    
    @PostConstruct
    void init(){
            dbref = SessionUtil.getCookieDBRef();
            renderOp1 = true;
            renderOp2 = false;
        try {
            listUserAccount = accountsession.getRef();
            listAppMenu = appSession.getAllAppMenu(dbref);
            listUserGroup = groupsession.getAllGroup(dbref);
            
        } catch (Exception ex) {
            Logger.getLogger(ConfigMenuMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
   
    }
    
    public void setConfig(){
            renderOp1 = false;
            renderOp2 = true;
    }
    
    public void onChangeGroup(){
        if (idusergroup !=0) {
             try {
                listMenuMaster = menusession.getAllMasterMenuByGroup(idusergroup, dbref);
            } catch (Exception ex) {
                Logger.getLogger(ConfigMenuMBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            MessageDialog.addMessageWarn("Pilih Group dahulu");
        }
       
    }
    
    public void simpan(){
        if (idusergroup !=0) {
          for (int i = 0; i < selectedMultiMasterMenu.size(); i++) {
              AppMenu ap = new AppMenu(idusergroup,
                            selectedMultiMasterMenu.get(i).getMasterkey(),
                            selectedMultiMasterMenu.get(i).isRead(),
                            selectedMultiMasterMenu.get(i).isWrite(),
                            selectedMultiMasterMenu.get(i).isPrinting(),
                            selectedMultiMasterMenu.get(i).isExternalAccess()
                            );
              try {
                  
                  appSession.saveAppMenu(ap, dbref);
                  init();
              } catch (Exception ex) {
                  Logger.getLogger(ConfigMenuMBean.class.getName()).log(Level.SEVERE, null, ex);
              }
        }  
        }else{
            MessageDialog.addMessageWarn("Group Belum dipilih");
        }
        
        
        
    }
    public void clear(){
        
    }
    public void cancel(){
        setRenderOp1(true);
        setRenderOp2(false);
    }
    
    public void hapus(){
        
        try {
            if (dbref !=null) {
                appSession.deleteAppMenu(selectAppMenu.getUserGroup(),selectAppMenu.getMenuMaster(),dbref);
                init();
                MessageDialog.addMessageInfo("Menu Group "+groupname+" Sudah diUbah");
            }else{
                MessageDialog.addMessageWarn("DB Ref belum dipilih \nEdit dibatalkan");
            }
        } catch (Exception e) {
        }
        
    }
    
    public void selectDBRef(){
        try {
            listAppMenu = appSession.getAllAppMenu(dbref);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    public void onRowEdit(RowEditEvent event){
        AppMenu appm = (AppMenu)event.getObject();
        try {
            if (dbref !=null) {
                appSession.updateAppMenu(appm,dbref);
                init();
                MessageDialog.addMessageInfo("Menu Group "+appm.getGroupname()+" Sudah diUbah");
            }else{
                MessageDialog.addMessageWarn("DB Ref belum dipilih \nEdit dibatalkan");
            }
        } catch (Exception e) {
        }
        
            
    }
    
    public void onRowCancel(RowEditEvent event){
        FacesMessage msg = new FacesMessage("Batal Edit Group ",((AppMenu)event.getObject()).getGroupname());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void onChange(){
        
    }
    
//    public void onReadChecked(){
//        
//        boolean summary = isread ? true : false;
//        setIsread(summary);
//    }
    
    public String getDbref() {
        return dbref;
    }

    public void setDbref(String dbref) {
        this.dbref = dbref;
    }

    public boolean isRenderOp1() {
        return renderOp1;
    }

    public void setRenderOp1(boolean renderOp1) {
        this.renderOp1 = renderOp1;
    }

    public boolean isRenderOp2() {
        return renderOp2;
    }

    public void setRenderOp2(boolean renderOp2) {
        this.renderOp2 = renderOp2;
    }
    
    
    public List<MenuMaster> getListMenuMaster() {
        return listMenuMaster;
    }

    public void setListMenuMaster(List<MenuMaster> listMenuMaster) {
        this.listMenuMaster = listMenuMaster;
    }

    public List<UserGroup> getListUserGroup() {
        return listUserGroup;
    }

    public void setListUserGroup(List<UserGroup> listUserGroup) {
        this.listUserGroup = listUserGroup;
    }

    public MenuMaster getMenuMaster() {
        return menuMaster;
    }

    public void setMenuMaster(MenuMaster menuMaster) {
        this.menuMaster = menuMaster;
    }

    public UserGroup getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(UserGroup userGroup) {
        this.userGroup = userGroup;
    }

    public UserGroup getSelectUserGroup() {
        return selectUserGroup;
    }

    public void setSelectUserGroup(UserGroup selectUserGroup) {
        this.selectUserGroup = selectUserGroup;
    }

    public MenuMaster getSelectMenuMaster() {
        return selectMenuMaster;
    }

    public void setSelectMenuMaster(MenuMaster selectMenuMaster) {
        this.selectMenuMaster = selectMenuMaster;
    }

    public List<AppMenu> getListAppMenu() {
        return listAppMenu;
    }

    public void setListAppMenu(List<AppMenu> listAppMenu) {
        this.listAppMenu = listAppMenu;
    }

    public AppMenu getAppMenu() {
        return appMenu;
    }

    public void setAppMenu(AppMenu appMenu) {
        this.appMenu = appMenu;
    }

    public AppMenu getSelectAppMenu() {
        return selectAppMenu;
    }

    public void setSelectAppMenu(AppMenu selectAppMenu) {
        this.selectAppMenu = selectAppMenu;
    }

    public int getIdmenu() {
        return idmenu;
    }

    public void setIdmenu(int idmenu) {
        this.idmenu = idmenu;
    }

    public int getIdusergroup() {
        return idusergroup;
    }

    public void setIdusergroup(int idusergroup) {
        this.idusergroup = idusergroup;
    }

    public String getMenuname() {
        return menuname;
    }

    public void setMenuname(String menuname) {
        this.menuname = menuname;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public boolean isIsread() {
        return isread;
    }

    public void setIsread(boolean isread) {
        this.isread = isread;
    }

    public boolean isIswrite() {
        return iswrite;
    }

    public void setIswrite(boolean iswrite) {
        this.iswrite = iswrite;
    }

    public boolean isIsprinting() {
        return isprinting;
    }

    public void setIsprinting(boolean isprinting) {
        this.isprinting = isprinting;
    }

    public boolean isIsexternalaccess() {
        return isexternalaccess;
    }

    public void setIsexternalaccess(boolean isexternalaccess) {
        this.isexternalaccess = isexternalaccess;
    }

    public List<UserAccount> getListUserAccount() {
        return listUserAccount;
    }

    public void setListUserAccount(List<UserAccount> listUserAccount) {
        this.listUserAccount = listUserAccount;
    }

    public List<AppMenu> getSelectedMultiAppMenu() {
        return selectedMultiAppMenu;
    }

    public void setSelectedMultiAppMenu(List<AppMenu> selectedMultiAppMenu) {
        this.selectedMultiAppMenu = selectedMultiAppMenu;
    }

    public List<MenuMaster> getSelectedMultiMasterMenu() {
        return selectedMultiMasterMenu;
    }

    public void setSelectedMultiMasterMenu(List<MenuMaster> selectedMultiMasterMenu) {
        this.selectedMultiMasterMenu = selectedMultiMasterMenu;
    }
    
    
    
}
