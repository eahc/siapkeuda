package com.web.keuda.utilities;

import com.web.utilities.DefaultExceptionHandler;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

/**
 *
 * @author Tri
 */
public class DefaultExceptionHandlerFactory extends ExceptionHandlerFactory{

    private ExceptionHandlerFactory parent;
    
    public DefaultExceptionHandlerFactory(ExceptionHandlerFactory parent) {
	        this.parent = parent;
	    }
    
    @Override
    public ExceptionHandler getExceptionHandler() {               
        ExceptionHandler eh = parent.getExceptionHandler();
	        eh = new DefaultExceptionHandler(eh);
	 
	        return eh;
    }

}
