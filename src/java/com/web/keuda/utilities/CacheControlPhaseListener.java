package com.web.keuda.utilities;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author anchii
 */
public class CacheControlPhaseListener implements PhaseListener {

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RENDER_RESPONSE;
    }

    @Override
    public void afterPhase(PhaseEvent event) {
    }

    @Override
    public void beforePhase(PhaseEvent event) {

        FacesContext facesContext = event.getFacesContext();
        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
//        response.addHeader("Pragma", "no-cache");
//        response.addHeader("Cache-Control", "no-cache");
//        // Stronger according to blog comment below that references HTTP spec
//        response.addHeader("Cache-Control", "no-store");
//        response.addHeader("Cache-Control", "must-revalidate");
//        // some date in the past
//        response.addHeader("Expires", "0");
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.setDateHeader("Expires", 0);
    }
}
